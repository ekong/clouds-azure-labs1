from flask import Flask, request, render_template_string
import numpy as np

app = Flask(__name__)

def numerical_integration(func, lower, upper, N):
    width = (upper - lower) / N
    total_area = 0
    for i in range(N):
        height = func(lower + i * width)
        total_area += height * width
    return total_area

@app.route('/', methods=['GET', 'POST'])
def integrate():
    results = {}
    if request.method == 'POST':
        try:
            lower = request.form.get('lower')
            upper = request.form.get('upper')

            # Interpret 'pi' as np.pi
            lower = np.pi if lower.lower() == 'pi' else float(lower)
            upper = np.pi if upper.lower() == 'pi' else float(upper)

            # Calculate integration for various values of N
            for N in [10, 100, 1000, 10000, 100000, 1000000]:
                result = numerical_integration(lambda x: np.abs(np.sin(x)), lower, upper, N)
                results[N] = result
        except (ValueError, TypeError):
            results = "Invalid input. Please enter numerical values or 'pi'."

    return render_template_string("""
        <!DOCTYPE html>
        <html>
        <head>
            <title>Numerical Integration</title>
        </head>
        <body>
            <h2>Numerical Integration of |sin(x)|</h2>
            <form method="post">
                Lower bound: <input type="text" name="lower"><br>
                Upper bound: <input type="text" name="upper"><br>
                <input type="submit" value="Integrate">
            </form>
            {% if results %}
                {% if results is string %}
                    <p>{{ results }}</p>
                {% else %}
                    {% for N, result in results.items() %}
                        <p>Result for N = {{ N }}: {{ result }}</p>
                    {% endfor %}
                {% endif %}
            {% endif %}
        </body>
        </html>
    """, results=results)

if __name__ == '__main__':
    app.run(debug=True)
